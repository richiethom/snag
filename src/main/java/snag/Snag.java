package snag;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Richard on 14/05/2015.
 */
public final class Snag<T, R> {

    public enum ReturnConfiguration {
        RETURN_NULL,
        RETURN_NEW_INSTANCE
    }
    private final MethodOperation<R, T> firstMethodOperation;

    public Snag(MethodOperation first) {
        this.firstMethodOperation = first;

    }

    public R get(T firstCar) {
        return (R) firstMethodOperation.get(firstCar);
    }

    private static class MethodOperation<R, T> {

        public void setNext(MethodOperation<?, ?> next) {
            this.next = next;
        }

        private MethodOperation<?, ?> next;

        private final Method declaredMethod;

        private final R returnInstance;

        public MethodOperation(final Method declaredMethod, R returnInstance) {
            this.declaredMethod = declaredMethod;
            this.returnInstance = returnInstance;
        }

        public Object get(Object currentInstance) {
            if (currentInstance==null) {
                return returnInstance;
            } else {
                try {
                    final Object result = declaredMethod.invoke(currentInstance);
                    if (result == null) {
                        return returnInstance;
                    } else if (next!=null) {
                        return next.get(result);
                    } else {
                        return result;
                    }
                } catch (IllegalAccessException e) {
                    return null;
                } catch (InvocationTargetException e) {
                    return null;
                }
            }
        }
    }

    public static <T, S> SnagConfiguration<T,S> createForAndReturn(Class<T> startClass, Class<S> returnClass) {

        return new SnagConfiguration<T, S>(startClass, returnClass);

    }

    public static class SnagConfiguration<T, S> {

        private final Class<T> startClass;
        private final Class<S> returnClass;
        private String path;

        private SnagConfiguration(Class<T> startClass, Class<S> returnClass) {
            this.startClass = startClass;
            this.returnClass = returnClass;
        }

        public SnagConfiguration<T, S> toGet(String path) {
            this.path = path;
            return this;
        }

        public Snag<T, S> andReturnNullIfMissing() {
            return doInstantiation(ReturnConfiguration.RETURN_NULL, null);

        }

        private Snag<T, S> doInstantiation(ReturnConfiguration returnType, Class<? extends S> concreteType) {
            final S returnInstance;
            try {
                switch (returnType) {
                    case RETURN_NEW_INSTANCE:
                        if (concreteType != null) {
                            returnInstance = concreteType.newInstance();
                        } else {
                            returnInstance = returnClass.newInstance();
                        }
                        break;
                    case RETURN_NULL:
                        returnInstance = null;
                        break;
                    default:
                        throw new IllegalStateException("Unrecognised returnConfiguration:" + returnType);
                }
            } catch (InstantiationException ie) {
                throw new IllegalStateException(ie);
            } catch (IllegalAccessException iae) {
                throw new IllegalStateException(iae);
            }
            final String[] fields = path.split("\\.");
            MethodOperation first = null;
            MethodOperation previous = null;
            Class<?> currentType = startClass;
            for (String field : fields) {
                final String methodName = "get"+Character.toUpperCase(field.charAt(0))+field.substring(1);
                try {
                    final Method declaredMethod = currentType.getDeclaredMethod(methodName);
                    currentType = declaredMethod.getReturnType();
                    final MethodOperation methodOperation = new MethodOperation(declaredMethod, returnInstance);
                    if (previous != null) {
                        previous.setNext(methodOperation);
                    } else {
                        first=methodOperation;
                    }
                    previous=methodOperation;
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                    throw new IllegalStateException(e);
                }
            }

            return new Snag<T, S>(first);
        }

        public Snag<T, S> andReturnANewInstanceIfMissing() {
            return andReturnANewInstanceIfMissing(returnClass);
        }

        public Snag<T, S> andReturnANewInstanceIfMissing(Class<? extends S> concreteType) {
            if (concreteType.isInterface()) {
                throw new IllegalStateException("The return class "+returnClass+" is an interface - try passing in a concrete type");
            }
            return doInstantiation(ReturnConfiguration.RETURN_NEW_INSTANCE, concreteType);
        }
    }

}
