package snagdemo;

import snag.Snag;
import snagdemo.models.Car;
import snagdemo.models.Door;
import snagdemo.models.Engine;
import snagdemo.models.Piston;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Richard on 14/05/2015.
 */
public class SnagDemo {

    private static final Snag<Car, String> ENGINE_NAME = Snag.createForAndReturn(Car.class, String.class).toGet("engine.name").andReturnNullIfMissing();

    private static final Snag<Car, ArrayList> DOORS_LIST = Snag.createForAndReturn(Car.class, ArrayList.class).toGet("doors").andReturnANewInstanceIfMissing();

    private static final Snag<Car, List> PISTONS_LIST = Snag.createForAndReturn(Car.class, List.class).toGet("engine.pistons").andReturnANewInstanceIfMissing(ArrayList.class);

    public static void main(String[] args) {
        final List<Piston> pistonList = new ArrayList<Piston>();
        pistonList.add(new Piston("bob"));
        final Car firstCar = new Car(new Engine("Caterham", pistonList), null);

        final long start1 = System.nanoTime();
        final String name =  ENGINE_NAME.get(firstCar);
        final long stop1 = System.nanoTime();

        final long start2 = System.nanoTime();
        final List<Door> arrayList = DOORS_LIST.get(firstCar);
        final long stop2 = System.nanoTime();

        final long start3 = System.nanoTime();
        final List<Piston> otherList = PISTONS_LIST.get(firstCar);
        final long stop3 = System.nanoTime();

        System.out.println(stop1 - start1);
        System.out.println(stop2 - start2);
        System.out.println(stop3 - start3);

        for (Piston p : otherList) {
            System.out.println(p.getName());
        }


    }


}
