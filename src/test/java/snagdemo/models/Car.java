package snagdemo.models;

import java.util.List;

/**
 * Created by Richard on 14/05/2015.
 */
public class Car {

    private final Engine engine;

    private final List<Door> doors;

    public Car(Engine engine, List<Door> doors) {
        this.engine = engine;
        this.doors = doors;
    }

    public Engine getEngine() {
        return engine;
    }

    public List<Door> getDoors() {
        return doors;
    }


}
