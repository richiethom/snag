package snagdemo.models;

/**
 * Created by Richard on 14/05/2015.
 */
public class Door {

    private final String name;

    public Door(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
