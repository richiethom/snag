package snagdemo.models;

import java.util.List;

/**
 * Created by Richard on 14/05/2015.
 */
public class Engine {

    private final String name;
    private final List<Piston> pistons;

    public Engine(final String name, List<Piston> pistons) {
        this.name = name;
        this.pistons = pistons;
    }

    public String getName() {
        return name;
    }

    public List<Piston> getPistons() {
        return pistons;
    }
}
