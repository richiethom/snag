package snagdemo.models;

/**
 * Created by Richard on 14/05/2015.
 */
public class Piston {

    private final String name;

    public Piston(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
